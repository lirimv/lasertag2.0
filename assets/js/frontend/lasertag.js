import "../../css/frontend/lasertag.scss";
import "bootstrap/dist/js/bootstrap.min";

import "./modules/smoothScroll";
import baguetteBox from 'baguettebox.js/dist/baguetteBox.min';

baguetteBox.run('.gallery');
