<?php

namespace App\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('frontend/home/index.html.twig');
    }

    /**
     * @Route("/lasertag", name="lasertag")
     */
    public function lasertagAction(): Response
    {
        return $this->render('frontend/home/lasertag.html.twig');
    }
}
